package com.devesh.example.Encapsulation;

public class Example {

	public static void main(String[] args) {
		String name = "Devesh";
		int age = 23;
		double salary = 28000.0;

		Employee employee = new Employee(name, age, salary);

		System.out.println("Name: " + employee.getName());
		System.out.println("Age: " + employee.getAge());
		System.out.println("Salary: " + employee.getSalary());
	}
}